import * as TYPES from '../actions/types';

const initialState = {
    uploading: false,
    changes: [],
    error: null
};

const configChange = (state = initialState, action) => {
    switch (action.type) {
        case TYPES.CHANGE_CONFIGURATION:
            return {...state, uploading: true, error: null, changes: []};
        case TYPES.CHANGE_CONFIGURATION_SUCCESS:
            return {...state, uploading: false, error: null, changes: action.payload};
        case TYPES.CHANGE_CONFIGURATION_ERROR:
            return {...state, uploading: false, error: action.payload, changes: []};
        default:
            return (state);
    }
};

export default configChange;
