import {routerReducer as routing} from 'react-router-redux';
import {combineReducers} from 'redux';

import fileUploader from './fileUploader';
import configChange from './configChange';

const rootReducer = combineReducers({
    fileUploader,
    configChange,
    routing
});

export default rootReducer;
