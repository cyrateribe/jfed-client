import * as TYPES from '../actions/types';

const initialState = {
    uploading: false,
    file: null,
    error: null
};

const fileUploader = (state = initialState, action) => {
    switch (action.type) {
        case TYPES.UPLOAD_FILE:
            return {...state, uploading: true, error: null, file: null};
        case TYPES.UPLOAD_FINISHED_SUCCESSFUL:
            return {...state, uploading: false, file: action.payload, error: null};
        case TYPES.UPLOAD_FINISHED_ERRORED:
            return {...state, uploading: false, file: null, error: action.payload};
        default:
            return (state);
    }
};

export default fileUploader;
