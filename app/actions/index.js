import * as TYPES from './types';

import {uploadFileService} from '../services/uploadFileService';
import {changeConfigurationService} from '../services/changeConfigurationService';

export function uploadingFile(bool) {
    return {
        type: TYPES.UPLOAD_FILE,
        payload: bool
    };
}

export function uploadFileError(error) {
    return {
        type: TYPES.UPLOAD_FINISHED_ERRORED,
        payload: error
    };
}

export function uploadFileSuccess(res) {
    return {
        type: TYPES.UPLOAD_FINISHED_SUCCESSFUL,
        payload: res
    };
}

export function changingConfiguration(bool) {
    return {
        type: TYPES.CHANGE_CONFIGURATION,
        payload: bool
    };
}

export function changeConfigurationSuccess(changes) {
    return {
        type: TYPES.CHANGE_CONFIGURATION_SUCCESS,
        payload: changes
    };
}

export function changeConfigurationError(err) {
    return {
        type: TYPES.CHANGE_CONFIGURATION_ERROR,
        payload: err
    };
}

const changeConfiguration = (fileName) => {
    return (dispatch) => {
        dispatch(changingConfiguration(true));
        setTimeout(() => {
            changeConfigurationService(fileName)
                .then(response => {
                    dispatch(changeConfigurationSuccess(response.data));
                })
                .catch(err => {
                    dispatch(changeConfigurationError(err.response.data));
                });
        }, 2000);
    };
};

export const uploadFileData = (data) => {
    return (dispatch) => {
        dispatch(uploadingFile(true));
        setTimeout(() => {
            uploadFileService(data)
                .then(response => {
                    dispatch(uploadFileSuccess(response.data.fileName));

                    dispatch(changeConfiguration(response.data.fileName));
                })
                .catch(err => {
                    dispatch(uploadFileError(err));
                });
        }, 2000);
    };
};
