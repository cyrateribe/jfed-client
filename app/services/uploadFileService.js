import axios from 'axios';

export const uploadFileService = (data) => {
    return axios({
        url: 'http://localhost:4000/uploadFile',
        method: 'post',
        data: data
    });
};
