import axios from 'axios';

export const changeConfigurationService = (fileName) => {
    return axios({
        url: 'http://localhost:4000/diff',
        method: 'GET',
        params: {fileName: fileName}
    });
};
