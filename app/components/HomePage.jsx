import React from 'react';
import {Container} from 'semantic-ui-react';

import FileUploader from './uploader/FileUploader';
import ChangeConfig from './configChange/ChangeConfig';

import '../styles/main.scss';

const HomePage = () =>
    <Container>
        <h1>JFED</h1>
        <FileUploader/>
        <ChangeConfig/>
    </Container>;

export default HomePage;
