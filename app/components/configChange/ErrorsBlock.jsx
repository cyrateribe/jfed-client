import React from 'react';
import PropTypes from 'prop-types';

const ErrorBlock = ({error}) => {
    return (
        <div>
            <p>{error}</p>
        </div>
    );
};

ErrorBlock.propTypes = {
    error: PropTypes.string
};

export default ErrorBlock;
