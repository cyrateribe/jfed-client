import React from 'react';
import {connect} from 'react-redux';
import {PropTypes} from 'prop-types';
import {Message} from 'semantic-ui-react';

import ErrorsBlock from './ErrorsBlock';
import Change from './Change';

const ChangeConfig = ({configChange}) => {
    return (
        <Message>
            <Message.Header><h3>Zmiany</h3></Message.Header>
            <Message.List>
                {
                    configChange.error !== null
                        ? <ErrorsBlock error={configChange.error}/>
                        : configChange.changes.map((change, i) =>
                            <Change key={i} change={change}/>
                        )
                }
            </Message.List>
        </Message>
    );
};

ChangeConfig.propTypes = {
    configChange: PropTypes.object
};

const mapStateToProps = (state) => {
    return {
        configChange: state.configChange
    };
};

export default connect(
    mapStateToProps,
)(ChangeConfig);
