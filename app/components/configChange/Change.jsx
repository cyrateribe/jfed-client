import React from 'react';
import PropTypes from 'prop-types';
import {Message} from 'semantic-ui-react';

const Change = ({change}) =>
    <Message.Item>{change}</Message.Item>;

Change.propTypes = {
    change: PropTypes.string
};

export default Change;
