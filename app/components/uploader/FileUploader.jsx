import React from 'react';
import {connect} from 'react-redux';
import {PropTypes} from 'prop-types';
import {Button, Form} from 'semantic-ui-react';

import {uploadFileData} from '../../actions/index';

const FileUploader = ({handleUpload}) => {
    let inputFile = React.createRef();

    function uploadClick(e) {
        e.preventDefault();
        const formData = new FormData();
        formData.file = inputFile.files[0];
        formData.append('file', inputFile.files[0]);
        if (!formData.file || formData.file.length <= 0) return;
        handleUpload(formData);
    }

    return (
        <div className="container">
            <Form>
                <Form.Field className="form-file-uploader">
                    <input ref={(ref) => {
                        inputFile = ref;
                    }} type="file" id="file-upl"/>
                    <label htmlFor="file-upl">Wybierz plik</label>
                </Form.Field>
                <Button className="btn upload-button" onClick={uploadClick}>Wyślij</Button>
            </Form>
        </div>
    );
};


FileUploader.propTypes = {
    handleUpload: PropTypes.func
};

const mapStateToProps = (state) => {
    return {
        fileUploader: state.fileUploader
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        handleUpload: file => dispatch(uploadFileData(file))
    };
};

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(FileUploader);

